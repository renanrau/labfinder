package com;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Usuario {
    // atributos
    private int id;
    private String nome;
    private char sexo;
    private String endereco;
    private LocalDate dt_nasc;
    private LocalDate dt_adm;
    private String email;
    private int salario;
    // construtor
    public Usuario(int id, String nome, char sexo, String endereco, LocalDate dt_nasc, LocalDate dt_adm, String email, int salario) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.endereco = endereco;
        this.dt_nasc = dt_nasc;
        this.dt_adm = dt_adm;
        this.email = email;
        this.salario = salario;
    }
    // métodos
    public String getNome() {
        return nome;
    }
    public String getDataNasc() {
        DateTimeFormatter padraoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return dt_nasc.format(padraoData);
    }
    public String getDataAdm() {
        DateTimeFormatter padraoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return dt_adm.format(padraoData);
    }
}
