package com;

public class Equipamento {
    // atributos
    private int id;
    private String descricao;
    private char tipo;
    private float valor_diario;
    private char status;
    // construtor
    public Equipamento(int id, String descricao, char tipo, float valor_diario, char status) {
        this.id = id;
        this.descricao = descricao;
        this.tipo = tipo;
        this.valor_diario = valor_diario;
        this.status = status;
    }
    // métodos
}
