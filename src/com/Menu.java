package com;

import java.sql.*;
import java.util.Scanner;

public class Menu {
    
    public Menu() throws SQLException {
        // method scope vars
        int option = 0;
        String termo = "";
        String query = null;
        
        // Scanner instance
        Scanner puts = new Scanner(System.in);
        
        // DbConnect instance
        DbConnect database = new DbConnect();
        
        // Main print
        System.out.println(
                "Bem-Vindo! Escolha uma das opções abaixo: "
                + "\n1 - Listar funcionários em ordem alfabética."
                + "\n2 - Buscar funcionário por nome."
                + "\n3 - Buscar equipamento pela descrição."
                + "\n4 - Fazer uma nova reserva"
                + "\n5 - Relatório de reservas futuras"
                //+ "\n6 - Visualizar quantidade de reservas de equipamento"
                //+ "\n7 - Listar funcionários, reservas e custo total"
        );
        
        while (option < 1 || option > 5)
        {
            try {
                option = puts.nextInt();
            } catch (java.util.InputMismatchException e) {
               System.out.println("Operação não permitida. A aplicação será finalizada.");
               System.exit(0);
            }
            
            switch(option)
            {
                case 1:
                    query = "select * from funcionarios order by nome asc";
                    break;
                case 2:
                    System.out.print("Digite o nome do funcionário à ser pesquisado: ");
                    termo = puts.next();
                    query = "select nome from funcionarios where upper(nome) like upper('%"+termo+"%')";
                    break;
                case 3:
                    System.out.print("Digite o nome do equipamento à ser pesquisado: ");
                    termo = puts.next();
                    query = "select descricao from equipamentos where upper(descricao) like upper('%"+termo+"%')";
                    break;
                case 4:
                    System.out.print("\nDigite o código do funcionário: ");
                    int codFunc = puts.nextInt();
                    System.out.print("\nDigite o código do produto à ser reservado: ");
                    int produto = puts.nextInt();
                    System.out.print("\nDigite a data inicial para reserva (Formato yyyy-mm-dd): ");
                    String diasInicio = puts.next();
                    System.out.print("\nDigite a data final para reserva (Formato yyyy-mm-dd): ");
                    String diasFinal = puts.next();
                    query = "insert into reservas (funcionario,equipamento,data_locacao,data_entrega)"
                            + "values ("+codFunc+","+produto+",to_date('"+diasInicio+"','yyyy-mm-dd'),to_date('"+diasFinal+"','yyyy-mm-dd'))";
                    break;
                case 5:
                    query = "select func.nome, equip.descricao, res.data_locacao, res.data_entrega "
                            + "from funcionarios func "
                            + "inner join reservas res "
                            + "on func.matricula = res.funcionario "
                            + "inner join equipamentos equip "
                            + "on res.equipamento = equip.registro";
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    break;
                default:
                    System.out.println("Opção inválida! Tente novamente!");
                    break;
            }
        }
        database.Search(false,option,query);
    }
}
