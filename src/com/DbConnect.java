package com;

import java.sql.*;

public class DbConnect {
    
    public void Search(boolean address, int type, String sql) throws SQLException {
        // set database connection (True => Local | False => PUCRS DB)
        String link[] = new String[2];
        if (address) {
            link[0] = "jdbc:derby://localhost:1527/labFinder";
            link[1] = "root";
        } else {
            link[0] = "jdbc:oracle:thin:@//camburi.pucrs.br:1521/facin11g";
            link[1] = "bg111244";
        }
        // instance to database connection
        Connection connect = DriverManager.getConnection(link[0],link[1],link[1]);
        // execute query
        PreparedStatement stmt = connect.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        
        // show query
        switch(type)
        {
            case 1:
                System.out.println("LISTANDO FUNCIONÁRIOS CADASTRADOS");
                while (rs.next()) {
                    String nome = rs.getString("nome");
                    System.out.println(nome);
                }
                break;
            case 2:
                System.out.println("\nFUNCIONÁRIOS ENCONTRADOS: ");
                while (rs.next()) {
                    String nome = rs.getString("nome");
                    System.out.println(nome);
                }
                break;
            case 3:
                System.out.println("\nEQUIPAMENTOS ENCONTRADOS: ");
                while (rs.next()) {
                    String descr = rs.getString("descricao");
                    System.out.println(descr);
                }
                break;
            case 4:
                System.out.println("\nReserva realizada com sucesso!");
                break;
            case 5:
                System.out.println("\nRelatório de Reservas: ");
                while (rs.next()) {
                    String nome = rs.getString("nome");
                    String descr = rs.getString("descricao");
                    String locacao = rs.getString("data_locacao");
                    String entrega = rs.getString("data_entrega");
                    System.out.println("\nFuncionário: "+nome+" | Equipamento: "+descr+" | De "+locacao+" à "+entrega+"");
                }
                break;
            case 6:
                
                break;
            case 7:
                
                break;
            case 8:
                
                break;
        }

        // close connection
        rs.close();
        stmt.close();
        connect.close();
    }
    
}
